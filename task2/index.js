function get_max_value()
{
    return parseInt(document.querySelector('input[name="set"]:checked').value);

    var element = document.getElementById('max_value');
    console.log(element.value);
    return parseInt(element.value);
}

function get_operator()
{
    return document.querySelector('input[name="operator"]:checked').value;
    return document.getElementById('operator').value;
}

function checkAnswer(op1, sign, op2, answer)
{
    if(sign == '+') {
        return op1 + op2 == answer;
    } else {
        return op1 - op2 == answer;
    }
}

function getInt(name)
{
    var element = document.getElementById(name);
    return parseInt(element.innerHTML);

}

function getAnswer()
{
    var element = document.getElementById('answer');
    return parseInt(element.value, 8)
}

function check()
{
    var op1 = getInt('op1');
    var op2 = getInt('op2');
    var sign = document.getElementById('sign').innerHTML;
    var answer = getAnswer();
    if (checkAnswer(op1, sign, op2, answer)) {
        document.getElementById('result').innerHTML = 'correct';
    } else {
        document.getElementById('result').innerHTML = 'wrong';
    }
}

function generate()
{
    var max_value = get_max_value();
    var sign = get_operator();
    var operand1 = Math.random() * max_value;
    if(sign === '-') {
        var operand2 = Math.random() * (max_value - operand1);
    } else {
        var operand2 = Math.random() * operand1;
    }

    operand1 = Math.round(operand1);
    operand2 = Math.round(operand2);

    document.getElementById('op1').innerHTML = operand1
    document.getElementById('op2').innerHTML = operand2
    document.getElementById('sign').innerHTML = sign;
    document.getElementById('answer').value = '';
    document.getElementById('result').innerHTML = '';
}

function add_num(x)
{
    document.getElementById('answer').value += x.toString();
}
