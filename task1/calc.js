var CalcState = {
    "firstNumber" : 1,
    "secondNumber" : 2,
    "showResult" : 3
};

Object.freeze(CalcState);
var calcState = CalcState.firstNumber;
var g_accum = 0;
var g_action = '';

function clear_input()
{
    document.getElementById('input').innerHTML = '0';
}

function get_current_input()
{
    var content = document.getElementById('input').innerHTML;
    return parseInt(content, 8);
}

function update_input(val)
{
    if (calcState == CalcState.showResult) {
        clear_input();
        calcState = CalcState.firstNumber;
    }

    var content = document.getElementById('input').innerHTML;
    var newContent = val.toString();
    if (content != '0') {
        newContent = content + newContent;
    }
    document.getElementById('input').innerHTML = newContent;
}

function update()
{
    var val = get_current_input();
    clear_input();
    if (g_action === '+') {
        g_accum += val;
    } else if (g_action === '-') {
        g_accum -= val;
    } else if (g_action === '*') {
        g_accum *= val;
    } else {
        g_accum = val;
    }
}

function btn_press(val)
{
    update_input(val);
}

function btn_clear()
{
    g_accum = 0;
    clear_input();
}

function btn_plus()
{
    update();
    g_action = '+';
}

function btn_minus()
{
    update();
    g_action = '-';
}

function btn_mul()
{
    update();
    g_action = '*';
}

function btn_equal()
{
    update();
    g_action = '';
    document.getElementById('input').innerHTML = g_accum.toString(8);
    calcState = CalcState.showResult;
}
